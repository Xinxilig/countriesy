import { ethers } from "hardhat";

async function main() {
  const Tools = await ethers.getContractFactory("Tools");
  const tools = await Tools.deploy();
  await tools.deployed();
  const CounriesyContract = await ethers.getContractFactory("Countriesy", {
    libraries: {
      Tools: `${tools.address}`,
    },
  });
  let countriesy = await CounriesyContract.deploy();
  await countriesy.deployed();
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
