// SPDX-License-Identifier: MIT
pragma solidity ^0.8.11;

import "./Deployer.sol";
import "./Tools.sol";

//import "hardhat/console.sol";

contract Countriesy is Deployer {
  //uint8 private numberOfCountries;
  string[] private continentList = [
    "Africa",
    "Antarctica",
    "Asia",
    "Europe",
    "North America",
    "Oceania",
    "South America"
  ];

  struct Country {
    string callingCode;
    string continent;
    string countryEnglishName;
    string currency;
    string flagEmoji;
    string isoCode;
    bool isCountryExist;
  }

  mapping(string => Country) private countriesMap;

  string[] internal countriesInDatabase;

  event ResponseMessage(string message);

  event Transaction(address indexed sender, uint256 amount);

  modifier badCountryCode(string memory _ISO2Code) {
    require(Tools.getAStringLength(_ISO2Code) == 2, "BadISO2Code");
    _;
  }

  modifier badCallingCode(string memory _callingCode) {
    require(Tools.getAStringLength(_callingCode) >= 2, "BadCallingCode");
    _;
  }

  modifier isCountryExist(string memory _ISO2Code) {
    require(
      !countriesMap[Tools.toUpperCase(_ISO2Code)].isCountryExist,
      "CountryAlreadyExist"
    );
    _;
  }

  modifier mustAlreadyExist(string memory _ISO2Code) {
    require(
      countriesMap[Tools.toUpperCase(_ISO2Code)].isCountryExist,
      "CountryMustAlreadyExist"
    );
    _;
  }

  modifier noCountryYet() {
    require(countriesInDatabase.length != 0, "NoCountryAddedYet");
    _;
  }

  modifier isContinentExist(
    string[] memory _continentList,
    string memory _continentName
  ) {
    require(
      Tools.isStringInArray(_continentList, _continentName) == true,
      "BadCountinentName"
    );
    _;
  }

  function getContinentList() external view returns (string[] memory) {
    return continentList;
  }

  //Function to get countries for a given continent...
  function getAContinentCountries(string memory continentName)
    external
    view
    isContinentExist(continentList, continentName)
    noCountryYet
    returns (Country[] memory)
  {
    uint256 countryCount;
    string[] memory _countriesInDatabase = countriesInDatabase;
    for (uint256 i = 0; i < _countriesInDatabase.length; i++) {
      if (
        Tools.compareTo(
          countriesMap[_countriesInDatabase[i]].continent,
          continentName
        ) == true
      ) {
        countryCount++;
      }
    }
    Country[] memory continentCountries = new Country[](countryCount);
    uint256 continentCountryCounter;
    if (countryCount != 0) {
      for (uint256 i = 0; i < _countriesInDatabase.length; i++) {
        if (
          Tools.compareTo(
            countriesMap[_countriesInDatabase[i]].continent,
            continentName
          ) == true
        ) {
          continentCountries[continentCountryCounter] = countriesMap[
            _countriesInDatabase[i]
          ];
          continentCountryCounter++;
        }
      }
    }
    return continentCountries;
  }

  //Function to get all stored countries on the Contract...
  function getAllCountries()
    external
    view
    noCountryYet
    returns (Country[] memory)
  {
    string[] memory _countriesInDatabase = countriesInDatabase;
    Country[] memory allCountries = new Country[](_countriesInDatabase.length);
    uint256 countryCounter;
    for (uint256 i = 0; i < _countriesInDatabase.length; i++) {
      allCountries[countryCounter] = countriesMap[_countriesInDatabase[i]];
      countryCounter++;
    }
    return allCountries;
  }

  //Function to get the number of countries stored on the Contract...
  function getNumberOfCountries() external view returns (uint256) {
    return countriesInDatabase.length;
  }

  function getACountry(string memory _countryISO2Code)
    external
    view
    badCountryCode(_countryISO2Code)
    mustAlreadyExist(_countryISO2Code)
    returns (Country memory)
  {
    string memory isoCode = Tools.toUpperCase(_countryISO2Code);
    return countriesMap[isoCode];
  }

  //Function to store a batch of a maximum 10 countries in the Contract at the same time...
  function storeABatchOfTenCountries(string[6][10] calldata countriesData)
    external
    isOwner
  {
    string[] memory _countriesInDatabase = countriesInDatabase;
    string[] memory _continentList = continentList;
    string memory iSO2Code;
    for (uint256 i = 0; i < countriesData.length; i++) {
      if (
        Tools.getAStringLength(countriesData[i][0]) < 2 ||
        Tools.getAStringLength(countriesData[i][5]) != 2
      ) {
        continue;
      }
      if (Tools.isStringInArray(_continentList, countriesData[i][1]) == false) {
        continue;
      }
      if (countriesMap[countriesData[i][5]].isCountryExist == true) {
        continue;
      }
      iSO2Code = Tools.toUpperCase(countriesData[i][5]);
      countriesMap[iSO2Code] = Country(
        countriesData[i][0],
        countriesData[i][1],
        countriesData[i][2],
        countriesData[i][3],
        countriesData[i][4],
        iSO2Code,
        true
      );
      if (
        Tools.isStringInArray(_countriesInDatabase, countriesData[i][5]) ==
        false
      ) {
        countriesInDatabase.push(countriesData[i][5]);
      }
    }
    emit ResponseMessage("Success");
  }

  /*
  --> Submitted continent name of the followiing function must be started with
  Upper case caracter and must be the same as one returned by the getContinentList
  function... 
  use ("Africa" or "North America"... ) not ("africa" or "North america"... )

  --> Single country should be submitted like ["+376", "Europe", "Andorra", "Euro", "🇦🇩", "AD"]
  */
  function addANewCountry(string[6] memory countryData)
    external
    isOwner
    badCountryCode(countryData[5])
    isCountryExist(countryData[5])
    isContinentExist(continentList, countryData[1])
  {
    string memory iSO2Code = Tools.toUpperCase(countryData[5]);
    countriesMap[iSO2Code] = Country(
      countryData[0],
      countryData[1],
      countryData[2],
      countryData[3],
      countryData[4],
      iSO2Code,
      true
    );
    string[] memory _countriesInDatabase = countriesInDatabase;
    if (Tools.isStringInArray(_countriesInDatabase, countryData[5]) == false) {
      countriesInDatabase.push(countryData[5]);
    }
    emit ResponseMessage("Success");
  }

  //The _newCountryName submitted in the following function must be in English...
  function updateACountryName(
    string memory _countryISO2Code,
    string memory _newCountryName
  )
    external
    isOwner
    badCountryCode(_countryISO2Code)
    mustAlreadyExist(_countryISO2Code)
  {
    countriesMap[Tools.toUpperCase(_countryISO2Code)]
      .countryEnglishName = _newCountryName;
    emit ResponseMessage("Success");
  }

  //Don't forget to put the "+" sign before the _newCountryCallingCode inside the submitted string
  //And make sure it is a valid Calling code...
  function updateACountryCallingCode(
    string memory _countryISO2Code,
    string memory _newCountryCallingCode
  )
    external
    isOwner
    badCountryCode(_countryISO2Code)
    badCallingCode(_newCountryCallingCode)
    mustAlreadyExist(_countryISO2Code)
  {
    countriesMap[Tools.toUpperCase(_countryISO2Code)]
      .callingCode = _newCountryCallingCode;
    emit ResponseMessage("Success");
  }

  function updateACountryCurrency(
    string memory _countryISO2Code,
    string memory _newCountryCurrency
  )
    external
    isOwner
    badCountryCode(_countryISO2Code)
    mustAlreadyExist(_countryISO2Code)
  {
    countriesMap[Tools.toUpperCase(_countryISO2Code)]
      .currency = _newCountryCurrency;
    emit ResponseMessage("Success");
  }

  /*
  --> Submitted _newCountryContinent name of the followiing function must be started with
  Upper case caracter and must be the same as the one returned by the getContinentList
  function... 
  use ("Africa" or "North America"... ) not ("africa" or "North america"... )
  */
  function updateACountryContinent(
    string memory _countryISO2Code,
    string memory _newCountryContinent
  )
    external
    isOwner
    isContinentExist(continentList, _newCountryContinent)
    badCountryCode(_countryISO2Code)
    mustAlreadyExist(_countryISO2Code)
  {
    countriesMap[Tools.toUpperCase(_countryISO2Code)]
      .continent = _newCountryContinent;
    emit ResponseMessage("Success");
  }

  //--> Please Make sure you submit the valid _newCountryFlagEmoji string...
  function updateACountryFlagEmoji(
    string memory _countryISO2Code,
    string memory _newCountryFlagEmoji
  )
    external
    isOwner
    badCountryCode(_countryISO2Code)
    mustAlreadyExist(_countryISO2Code)
  {
    countriesMap[Tools.toUpperCase(_countryISO2Code)]
      .flagEmoji = _newCountryFlagEmoji;
    emit ResponseMessage("Success");
  }

  function deleteACountry(string memory _countryISO2Code)
    external
    isOwner
    noCountryYet
    badCountryCode(_countryISO2Code)
    mustAlreadyExist(_countryISO2Code)
  {
    uint256 countryIndex;
    string memory ISO2Code = Tools.toUpperCase(_countryISO2Code);
    string[] memory _countriesInDatabase = countriesInDatabase;
    for (uint256 i = 0; i < _countriesInDatabase.length; i++) {
      if (Tools.compareTo(_countriesInDatabase[i], ISO2Code) == true) {
        countryIndex = i;
        break;
      }
    }
    countriesInDatabase[countryIndex] = countriesInDatabase[
      countriesInDatabase.length - 1
    ];
    countriesInDatabase.pop();
    delete countriesMap[ISO2Code];
    emit ResponseMessage("Success");
  }

  receive() external payable {
    emit Transaction(msg.sender, msg.value);
  }

  function withdrawCountriesyBalance() external isOwner {
    require(address(this).balance != 0, "EmptyBalance");
    (bool res, ) = payable(msg.sender).call{ value: address(this).balance }("");
    require(res, "TranXFailed");
    emit ResponseMessage("Success");
  }

  function getCountriesyBalance()
    external
    view
    isOwner
    returns (uint256 Balance)
  {
    Balance = address(this).balance;
  }
}
