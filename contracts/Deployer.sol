// SPDX-License-Identifier: MIT
pragma solidity ^0.8.11;

contract Deployer {
  address payable public immutable owner;

  constructor() {
    owner = payable(msg.sender);
  }

  modifier isOwner() {
    require(msg.sender == owner, "OnlyDeployer");
    _;
  }
}
