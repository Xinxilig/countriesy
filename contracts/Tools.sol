// SPDX-License-Identifier: MIT
pragma solidity ^0.8.11;

library Tools {
  function _upper(bytes1 _b1) private pure returns (bytes1) {
    if (_b1 >= 0x61 && _b1 <= 0x7A) {
      return bytes1(uint8(_b1) - 32);
    }
    return _b1;
  }

  function _lower(bytes1 _b1) private pure returns (bytes1) {
    if (_b1 >= 0x41 && _b1 <= 0x5A) {
      return bytes1(uint8(_b1) + 32);
    }
    return _b1;
  }

  //Function to change all caracters of a string to Lower case...
  function toLowerCase(string memory _base)
    public
    pure
    returns (string memory)
  {
    bytes memory _baseBytes = bytes(_base);
    for (uint256 i = 0; i < _baseBytes.length; i++) {
      _baseBytes[i] = _lower(_baseBytes[i]);
    }
    return string(_baseBytes);
  }

  //Function to change all caracters of a string to Upper case...
  function toUpperCase(string memory _base)
    public
    pure
    returns (string memory)
  {
    bytes memory _baseBytes = bytes(_base);
    for (uint256 i = 0; i < _baseBytes.length; i++) {
      _baseBytes[i] = _upper(_baseBytes[i]);
    }
    return string(_baseBytes);
  }

  //Function to compare 2 strings
  function compareTo(string memory _base, string memory _value)
    public
    pure
    returns (bool)
  {
    bytes memory _baseBytes = bytes(_base);
    bytes memory _valueBytes = bytes(_value);
    if (_baseBytes.length != _valueBytes.length) {
      return false;
    }
    for (uint256 i = 0; i < _baseBytes.length; i++) {
      if (_baseBytes[i] != _valueBytes[i]) {
        return false;
      }
    }
    return true;
  }

  //Function to get a giving string length
  function getAStringLength(string memory _str)
    public
    pure
    returns (uint256 len)
  {
    len = bytes(_str).length;
  }

  //function to check if a string exist inside an array...
  function isStringInArray(string[] memory _arr, string memory _str)
    public
    pure
    returns (bool)
  {
    for (uint256 i = 0; i < _arr.length; i++) {
      if (compareTo(_arr[i], _str) == true) {
        return true;
      }
    }
    return false;
  }

  //Function used to increment a number by 1...
  function incByOne(uint256 x) public pure returns (uint256) {
    unchecked {
      return x + 1;
    }
  }
}
