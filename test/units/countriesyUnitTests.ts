import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import chai from "chai";
import assertArrays from "chai-arrays";
import { ethers } from "hardhat";

const expect = chai.expect;
chai.use(assertArrays);

describe("All Countriesy Contract Tests", () => {
  let countriesy: any;
  let owner: SignerWithAddress;
  let addr1: SignerWithAddress;
  let addr2: SignerWithAddress;
  let addrs: SignerWithAddress[];

  beforeEach(async () => {
    [owner, addr1, addr2, ...addrs] = await ethers.getSigners();
    const Tools = await ethers.getContractFactory("Tools");
    const tools = await Tools.deploy();
    await tools.deployed();
    const CounriesyContract = await ethers.getContractFactory("Countriesy", {
      libraries: {
        Tools: `${tools.address}`,
      },
    });
    countriesy = await CounriesyContract.deploy();
    await countriesy.deployed();
  });

  it("Should return the first continent of continentlist as Africa", async () => {
    expect((await countriesy.getContinentList())[0]).to.equal("Africa");
  });

  it("Should return the last continent of continentlist as South America", async () => {
    const contiList = await countriesy.getContinentList();
    expect(contiList[contiList.length - 1]).to.equal("South America");
  });

  it("Should return number of continent of continentlist as 7", async () => {
    const contiList = await countriesy.getContinentList();
    expect(contiList.length.toString()).to.equal("7");
  });

  it("Should return the list of the 7 continents.", async () => {
    expect(await countriesy.getContinentList()).to.be.equalTo([
      "Africa",
      "Antarctica",
      "Asia",
      "Europe",
      "North America",
      "Oceania",
      "South America",
    ]);
  });

  it("Should revert the transaction", async () => {
    //if no country added yet
    await expect(countriesy.getAllCountries()).to.be.reverted;
    await expect(countriesy.getAContinentCountries()).to.be.reverted;
    await expect(countriesy.deleteACountry("AD")).to.be.reverted;

    //if a country not already inserted in the contract or bad ISOCode submitted
    await expect(countriesy.getACountry("AD")).to.be.reverted;
  });

  it("Should add a new country", async () => {
    expect(await countriesy.owner()).to.equal(owner.address);
    await expect(
      countriesy
        .connect(addr1)
        .addANewCountry(["+886", "Asia", "Taiwan", "Dollar", "🇹🇼", "TW"])
    ).to.be.reverted;
    expect(
      await countriesy.addANewCountry([
        "+886",
        "Asia",
        "Taiwan",
        "Dollar",
        "🇹🇼",
        "TW",
      ])
    ).to.emit(countriesy, "ResponseMessage");
    await expect(
      countriesy.addANewCountry([
        "+886",
        "Asia",
        "Taiwan",
        "Dollar",
        "🇹🇼",
        "TW",
      ])
    ).to.be.reverted;
    await expect(
      countriesy.addANewCountry(["+886", "Asia", "Taiwan", "Dollar", "🇹🇼", "T"])
    ).to.be.reverted;
    await expect(
      countriesy.addANewCountry(["+886", "Asi", "Taiwan", "Dollar", "🇹🇼", "TW"])
    ).to.be.reverted;
    expect((await countriesy.getNumberOfCountries()).toString()).to.equal("1");
    expect(
      await countriesy.addANewCountry([
        "+379",
        "Europe",
        "Vatican",
        "Euro",
        "🇻🇦",
        "VA",
      ])
    ).to.emit(countriesy, "ResponseMessage");
    expect((await countriesy.getNumberOfCountries()).toString()).to.equal("2");
  });

  it("Should add a batch of 10 country informations", async () => {
    await expect(
      countriesy.connect(addr2).storeABatchOfTenCountries([
        ["+46", "Europe", "Sweden", "Krona", "🇸🇪", "SE"],
        ["+65", "Asia", "Singapore", "Dollar", "🇸🇬", "SG"],
        ["+290", "Africa", "Saint Helena", "Pound", "🇸🇭", "SH"],
        ["+386", "Europe", "Slovenia", "Euro", "🇸🇮", "SI"],
        ["+47", "Europe", "Svalbard and Jan Mayen", "Krone", "🇸🇯", "SJ"],
        ["+421", "Europe", "Slovakia", "Euro", "🇸🇰", "SK"],
        ["+232", "Africa", "Sierra Leone", "Leone", "🇸🇱", "SL"],
        ["+378", "Europe", "San Marino", "Euro", "🇸🇲", "SM"],
        ["+221", "Africa", "Senegal", "Franc", "🇸🇳", "SN"],
        ["+252", "Africa", "Somalia", "Shilling", "🇸🇴", "SO"],
      ])
    ).to.be.reverted;
    expect(
      await countriesy.storeABatchOfTenCountries([
        ["+46", "Europe", "Sweden", "Krona", "🇸🇪", "SE"],
        ["+65", "Asia", "Singapore", "Dollar", "🇸🇬", "SG"],
        ["+290", "Africa", "Saint Helena", "Pound", "🇸🇭", "SH"],
        ["+386", "Europe", "Slovenia", "Euro", "🇸🇮", "SI"],
        ["+47", "Europe", "Svalbard and Jan Mayen", "Krone", "🇸🇯", "SJ"],
        ["+421", "Europe", "Slovakia", "Euro", "🇸🇰", "SK"],
        ["+232", "Africa", "Sierra Leone", "Leone", "🇸🇱", "SL"],
        ["+378", "Europe", "San Marino", "Euro", "🇸🇲", "SM"],
        ["+221", "Africa", "Senegal", "Franc", "🇸🇳", "SN"],
        ["+252", "Africa", "Somalia", "Shilling", "🇸🇴", "SO"],
      ])
    ).to.emit(countriesy, "ResponseMessage");
    expect((await countriesy.getNumberOfCountries()).toString()).to.equal("10");
  });
  it("Should skip countries with bad [callingCode,continentName or ISO2Code] when adding a batch of 10 country informations", async () => {
    expect(
      await countriesy.storeABatchOfTenCountries([
        ["+46", "Europe", "Sweden", "Krona", "🇸🇪", "S"],
        ["+65", "Asia", "Singapore", "Dollar", "🇸🇬", "SG"],
        ["+290", "Africa", "Saint Helena", "Pound", "🇸🇭", "SH"],
        ["+386", "Europe", "Slovenia", "Euro", "🇸🇮", "SI"],
        ["+47", "Euroe", "Svalbard and Jan Mayen", "Krone", "🇸🇯", "SJ"],
        ["+421", "Europe", "Slovakia", "Euro", "🇸🇰", "SK"],
        ["+232", "Africa", "Sierra Leone", "Leone", "🇸🇱", "SL"],
        ["+378", "Europe", "San Marino", "Euro", "🇸🇲", "SM"],
        ["+", "Africa", "Senegal", "Franc", "🇸🇳", "SN"],
        ["+252", "Africa", "Somalia", "Shilling", "🇸🇴", "SO"],
      ])
    ).to.emit(countriesy, "ResponseMessage");
    await expect(countriesy.getACountry("SE")).to.be.reverted;
    await expect(countriesy.getACountry("SJ")).to.be.reverted;
    await expect(countriesy.getACountry("SN")).to.be.reverted;
    expect((await countriesy.getNumberOfCountries()).toString()).to.equal("7");
  });

  it("Should update a country informations", async () => {
    await expect(countriesy.updateACountryName("TW", "Taiwan")).to.be.reverted;
    await expect(countriesy.updateACountryCallingCode("TW", "+886")).to.be
      .reverted;
    await expect(countriesy.updateACountryCurrency("TW", "Euro")).to.be
      .reverted;
    await expect(countriesy.updateACountryContinent("TW", "Asia")).to.be
      .reverted;
    await expect(countriesy.updateACountryFlagEmoji("TW", "🇹🇼")).to.be.reverted;
    expect(
      await countriesy.addANewCountry([
        "+886",
        "Asia",
        "Taiwan",
        "Dollar",
        "🇹🇼",
        "TW",
      ])
    ).to.emit(countriesy, "ResponseMessage");
    expect((await countriesy.getNumberOfCountries()).toString()).to.equal("1");
    await expect(countriesy.updateACountryName("VA", "Vatican")).to.be.reverted;
    expect(await countriesy.updateACountryName("TW", "Thaiwan")).to.emit(
      countriesy,
      "ResponseMessage"
    );

    await expect(countriesy.updateACountryCallingCode("TW", "1")).to.be
      .reverted;
    await expect(countriesy.updateACountryCallingCode("VA", "+235")).to.be
      .reverted;
    expect(await countriesy.updateACountryCallingCode("TW", "+1")).to.emit(
      countriesy,
      "ResponseMessage"
    );

    await expect(countriesy.updateACountryCurrency("T", "Euro")).to.be.reverted;
    await expect(countriesy.updateACountryCurrency("VA", "Dollar")).to.be
      .reverted;
    expect(await countriesy.updateACountryCurrency("TW", "Euro")).to.emit(
      countriesy,
      "ResponseMessage"
    );

    await expect(countriesy.updateACountryContinent("T", "Asia")).to.be
      .reverted;
    await expect(countriesy.updateACountryContinent("VA", "Europe")).to.be
      .reverted;
    expect(await countriesy.updateACountryContinent("TW", "Asia")).to.emit(
      countriesy,
      "ResponseMessage"
    );

    await expect(countriesy.updateACountryFlagEmoji("TWN", "🇹🇼")).to.be
      .reverted;
    await expect(countriesy.updateACountryFlagEmoji("VA", "🇻🇦")).to.be.reverted;
    expect(await countriesy.updateACountryFlagEmoji("TW", "🇹🇼")).to.emit(
      countriesy,
      "ResponseMessage"
    );
  });

  it("Should delete a country", async () => {
    await expect(countriesy.deleteACountry("VA")).to.be.reverted;
    expect(
      await countriesy.addANewCountry([
        "+379",
        "Europe",
        "Vatican",
        "Euro",
        "🇻🇦",
        "va",
      ])
    ).to.emit(countriesy, "ResponseMessage");
    expect((await countriesy.getNumberOfCountries()).toString()).to.equal("1");
    await expect(countriesy.connect(addr2).deleteACountry("VA")).to.be.reverted;
    await expect(countriesy.deleteACountry("AD")).to.be.reverted;
    await expect(countriesy.deleteACountry("V")).to.be.reverted;
    await expect(countriesy.deleteACountry("VAA")).to.be.reverted;
    expect(await countriesy.deleteACountry("VA")).to.emit(
      countriesy,
      "ResponseMessage"
    );
    expect(
      (await countriesy.connect(addr1).getNumberOfCountries()).toString()
    ).to.equal("0");
    await expect(countriesy.deleteACountry("VA")).to.be.reverted;
  });

  it("Should tranfert some ethers to this contract", async () => {
    expect(await countriesy.getCountriesyBalance()).to.equal(
      ethers.BigNumber.from(0)
    );
    await owner.sendTransaction({
      to: countriesy.address,
      value: ethers.utils.parseEther("50.0"),
    });
    expect((await countriesy.getCountriesyBalance()).toString()).to.equal(
      ethers.utils.parseEther("50.0")
    );
  });

  it("Should allow only the deployer to withdraw money from this contract", async () => {
    await expect(countriesy.withdrawCountriesyBalance()).to.be.reverted;
    await addr1.sendTransaction({
      to: countriesy.address,
      value: ethers.utils.parseEther("50.0"),
    });
    await expect(countriesy.connect(addr2).withdrawCountriesyBalance()).to.be
      .reverted;
    expect(await countriesy.withdrawCountriesyBalance()).to.emit(
      countriesy,
      "ResponseMessage"
    );
    await expect(countriesy.withdrawCountriesyBalance()).to.be.reverted;
    expect(await countriesy.getCountriesyBalance()).to.equal(
      ethers.BigNumber.from(0)
    );
  });
});
