/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */

import { ethers } from "ethers";
import {
  FactoryOptions,
  HardhatEthersHelpers as HardhatEthersHelpersBase,
} from "@nomiclabs/hardhat-ethers/types";

import * as Contracts from ".";

declare module "hardhat/types/runtime" {
  interface HardhatEthersHelpers extends HardhatEthersHelpersBase {
    getContractFactory(
      name: "Countriesy",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.Countriesy__factory>;
    getContractFactory(
      name: "Deployer",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.Deployer__factory>;
    getContractFactory(
      name: "Greeter",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.Greeter__factory>;
    getContractFactory(
      name: "Tools",
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<Contracts.Tools__factory>;

    getContractAt(
      name: "Countriesy",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.Countriesy>;
    getContractAt(
      name: "Deployer",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.Deployer>;
    getContractAt(
      name: "Greeter",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.Greeter>;
    getContractAt(
      name: "Tools",
      address: string,
      signer?: ethers.Signer
    ): Promise<Contracts.Tools>;

    // default types
    getContractFactory(
      name: string,
      signerOrOptions?: ethers.Signer | FactoryOptions
    ): Promise<ethers.ContractFactory>;
    getContractFactory(
      abi: any[],
      bytecode: ethers.utils.BytesLike,
      signer?: ethers.Signer
    ): Promise<ethers.ContractFactory>;
    getContractAt(
      nameOrAbi: string | any[],
      address: string,
      signer?: ethers.Signer
    ): Promise<ethers.Contract>;
  }
}
