# ADD, GET and UPDATE continents and countries data easily on Ethereum Blockchain

**Countriesy** is a smart contract that put main focus on making getting, adding and updating countries and continents data much more simpler. It's written in _Solidity_ and _TypeScript_. You can use the _Hardhat_ framework to deploy it. If you want to play with it, **Countriesy** can be deployed to any testnet.The contract is also _Payable_ 💰.

_I recommend that you open this README in another tab as you perform the tasks below._

---

## Clone, configure your deployment network, share, deploy and Enjoy...😄

_First, you need to change your environment variables in the `.env` file for your specific deploiement network which you can setup in your depoiement framework config file_

_For Example, for the `Hardhat` framework using `Ropsten` network, set it up like this :_

```text
const config: HardhatUserConfig = {
  solidity: {
    version: "0.8.11",
    settings: {
      optimizer: {
        enabled: true,
        runs: 1000,
      },
    },
  },
  defaultNetwork: "hardhat",
  networks: {
    hardhat: {},
    ropsten: {
      url: process.env.ROPSTEN_URL,
      accounts: [`0x${process.env.PRIVATE_KEY}`],
    },
  },
};
```

_And use this command to deploy it to the`Ropsten` network :_

```text
npx hardhat run scripts/deploy.ts --network ropsten
```

_All about_ `Hardhat` [Here](https://www.sourcetreeapp.com/).

---

## Usage...🧰🧑‍💻

All countries data are in the ./data/countriesData.json file. the file contains 242 countries stored in an array of array of 10 countries. they are alreaty formatted and you can use them to add a country or add a batch of 10 countries at the same time...

- _Function to gat all continents list..._
  `getContinentList()`

- _Function to get countries for a given continent..._
  `getAContinentCountries(continentName)` where `continentName` is one of those you get from the `getContinentList()` function.

- _Function to get all stored countries on the Contract..._
  `getAllCountries()`

- _Function to get the number of countries stored on the Contract..._
  `getNumberOfCountries()`

- _Function to get a country stored on the Contract..._
  `getACountry(_countryISO2Code)` where `_countryISO2Code` is the searched country ISO2 code like `"DE"`...

- _Function to store a batch of a maximum 10 countries in the Contract at the same time..._
  `storeABatchOfTenCountries(countriesData)` where `countriesData` is an array of a batch of 10 countries that you can find in the ./data/countriesData.json file...

  _like this :_

```text
storeABatchOfTenCountries([
    ["+376", "Europe", "Andorra", "Euro", "🇦🇩", "AD"],
    ["+971", "Asia", "United Arab Emirates", "Dirham", "🇦🇪", "AE"],
    ["+93", "Asia", "Afghanistan", "Afghani", "🇦🇫", "AF"],
    ["+1268", "North America", "Antigua and Barbuda", "Dollar", "🇦🇬", "AG"],
    ["+1264", "North America", "Anguilla", "Dollar", "🇦🇮", "AI"],
    ["+355", "Europe", "Albania", "Lek", "🇦🇱", "AL"],
    ["+374", "Asia", "Armenia", "Dram", "🇦🇲", "AM"],
    ["+599", "North America", "Netherlands Antilles", "Guilder", "🇧🇶", "AN"],
    ["+244", "Africa", "Angola", "Kwanza", "🇦🇴", "AO"],
    ["+672", "Antarctica", "Antarctica", "", "🇦🇶", "AQ"]
  ])
```

- _Function to Add a new country..._
  _Submitted continent name of the followiing function must be started with Upper case caracter and must be the same as ones returned by the `getContinentList()` function... use "Africa" or "North America"... not "africa" or "North america"..._
  _Single country should be submitted like ["+376", "Europe", "Andorra", "Euro", "🇦🇩", "AD"]_

  `addANewCountry(countryData)` where `countryData` is an array of a specific country that you can find in the ./data/countriesData.json file...

  _like this :_

```text
addANewCountry(["+374", "Asia", "Armenia", "Dram", "🇦🇲", "AM"])
```

- _Function to update a specific country name... The `_newCountryName` submitted in the following function must be in English..._
  `updateACountryName( _countryISO2Code, _newCountryName )` where `_countryISO2Code` is the country ISO2 code like `"DE"`...

- _Function to update a specific country Calling Code... Don't forget to put the "+" sign before the `_newCountryCallingCode` inside the submitted string... And make sure it is a valid Calling code..._
  `updateACountryCallingCode( _countryISO2Code, _newCountryCallingCode )` where `_countryISO2Code` is the country ISO2 code like `"DE"`...

- _Function to update a specific country Currency..._
  `updateACountryCurrency( _countryISO2Code, _newCountryCurrency )` where `_countryISO2Code` is the country ISO2 code like "DE"`...

- _Function to update a specific country Continent name..._
  _Submitted continent name of the followiing function must be started with Upper case caracter and must be the same as ones returned by the `getContinentList()` function... use "Africa" or "North America"... not "africa" or "North america"..._
  `updateACountryContinent( _countryISO2Code, _newCountryContinent )` where `_countryISO2Code` is the country ISO2 code like "DE"`...

- _Function to delete a country from the Contract... Please Make sure you submit the valid `_newCountryFlagEmoji` string..._
  `updateACountryFlagEmoji( _countryISO2Code, _newCountryFlagEmoji )` where `_countryISO2Code` is the country ISO2 code like `"DE"`...

- _Function to delete a country from the Contract..._
  `deleteACountry(_countryISO2Code)`
  where `_countryISO2Code` is the country ISO2 code like `"DE"`...

---

## Additionals _(Only for the contract deployer)_

- _Function to withdraw funds from the Countriesy contract Balance..._
  `withdrawCountriesyBalance()`

- _Function to get the amount in Countriesy contract Balance..._
  `getCountriesyBalance()`

---

## Meet the DEV...🤼‍♂️

Interest to get more of me or socialize, here is [My linkedIn link](https://www.linkedin.com/in/perspicace-montcho-03b942208/). If you prefer, you can drop me a **Mail** to _blockchaindevxinxi@gmail.com_.

---

## DONATE...🥇

This contract is _Payable_. I code it that way cause if it is deployed to the _Mainnet_, deploiement and adding countries will cost some GAS. So if you find it useful, it might be kind to _Donate_ or _Buy a Bear_ to the Deployer...
